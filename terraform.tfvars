group_f5_mytsel_nomad  = [
    {
        name = "nomad_ingreess1"
        type                 = "iprange"
        start_ip             = "10.20.20.1"
        end_ip               = "10.20.20.5"
    },
     {
        name = "nomad_ingreess2"
        type                 = "iprange"
        start_ip             = "10.20.20.6"
        end_ip               = "10.20.20.8"
    
    },
     {
        name = "nomad_ingreess3"
        type                 = "iprange"
        start_ip             = "10.20.20.11"
        end_ip               = "10.20.20.13"
    
    }

]

group_f5_nomad_esb  = [
     {
        name = "nomad_eggress1"
        type                 = "iprange"
        start_ip             = "10.20.20.11"
        end_ip               = "10.20.20.13"
    
    },
      {
        type                 = "iprange"
        name                 = "nomad_eggress2"
        start_ip             = "10.20.20.30"
        end_ip               = "10.20.20.40"
    
    }

]