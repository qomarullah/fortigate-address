terraform {
  required_providers {
    fortios = {
      source = "fortinetdev/fortios"
      version = "1.13.2"
    }
  }
}

# Configure the FortiOS Provider for FortiGate
provider "fortios" {
  hostname     = var.hostname
  token        = var.token
  insecure     = "true"
  #cabundlefile = "certificate.crt"
}

####################### current state for group ###############################
# current state for group_f5_mytsel_nomad
data "fortios_firewall_addrgrp" "group_f5_mytsel_nomad" {
  name = "group_f5_mytsel_nomad" 
  #visibility    = "enable"

}

# current state for group_f5_nomad_esb
data "fortios_firewall_addrgrp" "group_f5_nomad_esb" {
  name = "group_f5_nomad_esb" 
  #visibility    = "enable"

}

####################### modify member group ###############################
# resource changes add group group_f5_mytsel_nomad
resource "fortios_firewall_addrgrp" "group_f5_mytsel_nomad" {
  allow_routing = "disable"
  color         = 0
  exclude       = "disable"
  name          = "group_f5_mytsel_nomad"
  #visibility    = "enable"


  dynamic "member" {
      for_each = fortios_firewall_address.group_f5_mytsel_nomad
      content{
        name = member.value.name
      }
  }
}

# resource changes add group group_f5_nomad_esb
resource "fortios_firewall_addrgrp" "group_f5_nomad_esb" {
  allow_routing = "disable"
  color         = 0
  exclude       = "disable"
  name          = "group_f5_nomad_esb"
  #visibility    = "enable"


  dynamic "member" {
      for_each = fortios_firewall_address.group_f5_nomad_esb
      content{
        name = member.value.name
      }
  }

}


####################### modify member group ###############################
# resource modify member group_f5_mytsel_nomad
resource "fortios_firewall_address" "group_f5_mytsel_nomad" {
  count               = "${length(var.group_f5_mytsel_nomad)}"
  allow_routing        = "disable"
  associated_interface = "port2"
  color                = 3
  name                 = "${lookup(var.group_f5_mytsel_nomad[count.index], "name")}"
  end_ip               = "${lookup(var.group_f5_mytsel_nomad[count.index], "end_ip")}"
  #subnet              = "${lookup(var.group_f5_mytsel_nomad[count.index], "subnet")}"
  start_ip             = "${lookup(var.group_f5_mytsel_nomad[count.index], "start_ip")}"
  type                 = "${lookup(var.group_f5_mytsel_nomad[count.index], "type")}"
  #visibility           = "enable"
}


# resource modify member group_f5_nomad_esb
resource "fortios_firewall_address" "group_f5_nomad_esb" {
  count               = "${length(var.group_f5_nomad_esb)}"
  allow_routing        = "disable"
  associated_interface = "port2"
  color                = 3
  name                 = "${lookup(var.group_f5_nomad_esb[count.index], "name")}"
  end_ip               = "${lookup(var.group_f5_nomad_esb[count.index], "end_ip")}"
  #subnet              = "${lookup(var.group_f5_nomad_esb[count.index], "subnet")}"
  start_ip             = "${lookup(var.group_f5_nomad_esb[count.index], "start_ip")}"
  type                 = "${lookup(var.group_f5_nomad_esb[count.index], "type")}"
  #visibility           = "enable"
}



####################### output ###############################
output "group_f5_mytsel_nomad" {
  value = "${data.fortios_firewall_addrgrp.group_f5_mytsel_nomad .name}" 
}

output "group_f5_nomad_esb" {
  value = "${data.fortios_firewall_addrgrp.group_f5_nomad_esb .name}" 
}

