# Required variables.

variable "hostname" {
  type  = string
  description = ""
}

variable "token" {
  type  = string
  description = ""
}

variable "group_f5_mytsel_nomad" {
  type =  list
  default = []
}

variable "group_f5_nomad_esb" {
  type =  list
  default = []
}
